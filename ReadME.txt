# Clone project from bitbucket with
- git clone https://ashu4444@bitbucket.org/ashu4444/mindbowser-assignment.git

#open project folder with VS code or write code . in terminal inside project folder

#install all the dependencies or modules of project
- npm install

#start project with
- npm start

#for creating a build
- npm run build

#if you want to launch test runner 
- npm test

